import convertapi
import glob
import sys
import random
import os



out_dir='result_'+sys.argv[1].split('.')[0]

os.mkdir(out_dir)

convertapi.api_secret ='<APIKEY>'


input_file=sys.argv[1]


convertapi.convert('txt', {
    'File': input_file
},from_format= 'pdf').save_files('./'+out_dir+'/extract.txt')

try:
    convertapi.convert('extract-images', {
        'File': input_file
    },from_format= 'pdf').save_files('./'+out_dir+'/')
except:
    pass


text = open('./'+out_dir+'/extract.txt','r')
html='''
<!DOCTYPE html>
<html>
<head>
<title>Research Article</title>
</head>
<style>

.container{
    padding:20px 20%;
}

  .container > p:first-child{
    
      font-size:2rem;
    font-weight:bolder !important;
    
  }
  strong{
    font-weight:400 !important;
  }

  strong.sub-header{
    font-weight:bolder !important;
  }
</style><body><div class="container">'''

for t in text.readlines():
    if t=="\n":
        html+="<br/>"
    else:
        html+="<p>"+t+"</p>"

for img in glob.glob('./'+out_dir+'/*.jpg'):
    html+="<img src={source}></img>".format(source='./'+img.split('/')[-1])


html+="</div></body>"

output=open('./'+out_dir+'/extract.html','w')

output.write(html)
