# Accessible Documents 

## Method1: Convert PDF to HTML  

### Create [ConvertApi](https://www.convertapi.com/) Account

- copy your apikey to **extract.py**

### Install Dependencies
``` pip install glob convertapi```

### execute
``` python3 extract.py <file_name> ```

- the script above makes a total of two requests using convertapi's end points. 
- The first one fetches all text from pdf file
- The second one fethces all the images from pdf file
- this will generate a new directory **result** with **output.html**
- All the extracted images displayed at the end.

## Method2: Convert .tex to HTML

### use any latex templates to create your project 
- Install [LaTeXML](https://hackmd.io/@UoL-IWG/latexml)


### execute the following command in your project root directory
``` latexmlc <file_name>.tex --dest=output/index.html --javascript="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js?config=MML_HTMLorMML" ``` 

- This will generate HTML file
- Copy the CSS **CustomStyle.css** from root directory of this repository to **ltx-article.css** file
- Copy the javascript **custom.js** from root directory of this repository and paste them between the script tags
```<script>{js}</script>```

### [sample file converted using LaTeXML with custom styling](https://2wwi7.csb.app/)

 
